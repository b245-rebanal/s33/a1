fetch("https://jsonplaceholder.typicode.com/todos")
		//The ".then()" method captures the response object and returns another promise which will be either resolved or rejected
		.then(response => response.json())
		.then(data => {
			let title = data.map(element => element.title)

			console.log(title);
		})

//GET method
fetch("https://jsonplaceholder.typicode.com/todos/1")
		.then(response => response.json())
		.then(result => {
			console.log(result);
			console.log(`This item "${result.title}" on the list has a status of false`)});



//POST method
fetch("https://jsonplaceholder.typicode.com/todos", {
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},

				body: JSON.stringify({
					completed: false,
					title: "Created To Do List Item",
					userId: 1
				})
			})
		.then(response => response.json())
		.then(result => console.log(result));


//PUT method

fetch("https://jsonplaceholder.typicode.com/todos/1", {
				method: "PUT",
				headers: {
					'Content-Type' : "application/json"
				},
				body: JSON.stringify({
					dateCompleted: "Pending",
					description: "To update the my to do list with a different data structure",
					status: "Pending",
					title: "Update To Do List Item",
					userId: 1
				})
		})
		.then(response => response.json())
		.then(result => console.log(result));


//PATCH method
fetch("https://jsonplaceholder.typicode.com/todos/1", {
			method: "PATCH",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				dateCompleted: "07/09/21",
				status:"Completed",
				title: "delectus aut autem"
			})
		})
		.then(response => response.json())
		.then(result => console.log(result));


//DELETE method
fetch("https://jsonplaceholder.typicode.com/todos/1", {method: "DELETE"})
		.then(response => response.json())
		.then(result => console.log(result));